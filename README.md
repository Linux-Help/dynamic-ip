# dynamic-ip

Dynamic IP automation tool.

A bash script that detects ipv4 and ipv6 IP's and automates running agents to handle them. Comes with agents to work with AWS Route53 and AWS Security Groups, but can work with anything.

# About

This tool detects and runs agents upon detection of changes of both IPv4 and IPv6 IP addresses. These agents can do a number of things, including update DNS records, setup a firewall to adjust changes, or practically anything you want.

I wrote this because I wanted a reliable means to handle being on a dynamic IP, but wanted the security of having the ability to update DNS records quickly, accurately, while also updating firewall rules such as with AWS Security Groups, Shorewall, etc, all automatically.

# Installation

This can be installed as root or as a regular non-root user. The instructions below will be assuming root, and on a system using systemd for init.

## For root:

```
mkdir /etc/dynamic-ip
rsync -avhHi src/ /etc/dynamic-ip/
cp init/dynamic-ip.timer init/dynamic-ip.target init/update-*.service /etc/systemd/system/
cp init/dynamic-ip.sysconfig /etc/sysconfig/dynamic-ip
systemctl daemon-reload
```

Edit /etc/sysconfig/dynamic-ip and set the DOMAIN_NAME to the domain of filename you wish.

## For user:

```
mkdir ~/.dynamic-ip
rsync -avhHi src/ ~/.dynamic-ip/
mkdir -p ~/.config/systemd/user/
cp init/dynamic-ip.timer init/dinamic-ip.target init/update-*.service ~/.config/systemd/user/
cp init/dynamic-ip.sysconfig ~/.dynamic-ip/config
```

Edit the ~/.config/systemd/user/update-ipv4.service and ~/.config/systemd/user/update-ipv6.service files and change EnvironmentFile to:

```
EnvironmentFile=$HOME/.dynamic-ip/config
```

Then edit $HOME/.dynamic-ip/config and set DOMAIN_NAME to the domain or filename you wish.


# Service and Timers

To configure the automated service portion of this, the services need to know what domain, or what domain file to use for it. That is defined in the domain-ip sysconfig file.

dynamic-ip can either take single domain name on the command-line, or a file of domain names listed per each line as the domains. In the sysconfig file you can define either, and dynamic-ip will auto-detect whether it's a file, if not, treat it as a domain name.

Once everything is configured, you can enable the services as follows:

## For root:

```
systemctl enable --now dynamic-ip.timer
systemctl enable update-ipv4.service
systemctl enable update-ipv6.service
```

This will enable and start the timer, and enable the services for the dynamic-ip.target.

## For user:

```
systemctl --user enable --now dynamic-ip.timer
systemctl --user enable update-ipv4.service
systemctl --user enable update-ipv6.service
```

This will enable and start the timer, and enable the services for the dynamic-ip.target for your user account.

# Plugins

dynamic-ip uses a plugin type architecture to allow you to define many other detections to perform, and how and when to perform them. These core plugins are stored in the dynamic-ip/plugins directory. update-ipv4 and ipdate-ipv6 are provided for checking for changes to IPv4 and IPv6 addresses and running the update.d agents upon such changes.

You can create your own plugins to do other detections. One such example would be to detect if a locally stored attribute variable form aother piece of software differs from the current DNS records. One such case this may be useful is with shorewall's params file, and if that differs, change the value in the file and re-run shorewall to apply the changes.

# Agents

The agents included work with Route53 and AWS Security Groups. Though you can use these as baselines for creating any other kind of agents for your own needs, these agents are fully functional for the purposes they were designed as-is. 

update-dns-r53 is designed to use cli53, a golang utility, to deal with Route53 DNS record updates. I chose this because it was the fastest and most reliable I'd seen, and could work with ZoneID's directly which can be important, especially when you have internal zones and external zones in Route53.

To configure it, you need to define your configuration in dynamic-ip's aws/config and aws/credentials for cli53 to use. And in conf.d/ you can configure, using the same name as update-dns-r53.conf, or you can use the exact name you link in update.d/, dynamic-ip reads the global one first, which is the same name plus .conf at the end, of the agent's original filename, and then if it exists, the linked name that was actually executed plus .conf at the end from the conf.d directory, so you can have multiple configurations as needed.

update-dns-r53 uses agent_update_dns_r53_id for the ZoneID or ZoneName.
aws-sg uses agent_aws_sg_id for the SGID.
